<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Process;


$app->post('/', function(Request $request) use ($app) {
  $code = $request->get('code');
  $input = $request->get('input');
  $prevdir = getcwd();
  $jaildir = __DIR__.'/../jail';
  $tmpdir = exec('mktemp -d -p '.$jaildir.'/tmp');
  $comp_pr = new Process('g++ -O2 -xc++ -', $tmpdir, array('PATH' => '/usr/bin'), $code);
  $comp_pr->run();
  exec('chmod -w '.$jaildir.'/tmp');
  $success = false;
  $stats = array();
  if ($comp_pr->isSuccessful()) {
    $exc_pr = new Process('cgexec -g memory:cep fakechroot chroot '.$jaildir.' /usr/bin/time /tmp/'.
      basename($tmpdir).'/a.out',
      $tmpdir, array('PATH' => '/usr/bin', 'TIME' => '%U %S %M'), $input, 2);
    //$exc_pr = new Process('/usr/bin/time ./a.out', $tmpdir, array('TIME' => '%U %S %M'), $input, 3);
    try {
      $exc_pr->run();
      if ($exc_pr->isSuccessful()) {
        $output = $exc_pr->getOutput();
        $success = true;
        $stats = explode("\n", $exc_pr->getErrorOutput());
        $stats = explode(' ', $stats[count($stats)-2]);
        $stats = array(floatval($stats[0]) + floatval($stats[1]), intval($stats[2]) / 1024.);
      } else {
        $output = explode("\n", $exc_pr->getErrorOutput());
        $output = implode("\n", array_slice($output, 0, count($output)-2));
      }
    } catch (\RuntimeException $e) {
      $output = 'Tiempo límite de 2 segundos excedido';
    }
  } else {
    $output = '';
  }
  exec('chmod +w '.$jaildir.'/tmp');
  exec('rm -r '.$tmpdir);
  return $app->json(array(
    'success' => $success,
    'output' => $output,
    'stats' => $stats,
    'compiler_msg' => $comp_pr->getErrorOutput()));
});

$app->error(function (\Exception $e, $code) use ($app) {
  if ($app['debug']) {
    return;
  }
  $page = 404 == $code ? '404.html' : '500.html';
  return new Response($app['twig']->render($page, array('code' => $code)), $code);
});

